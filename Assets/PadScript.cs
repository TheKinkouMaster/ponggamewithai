﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadScript : MonoBehaviour
{
    private Transform m_pad;
    public float m_leftBorder;
    public float m_rightBorder;
    public float m_speed;

    private void Awake()
    {
        m_pad = GetComponent<Transform>();
    }

    public void M_MoveRight()
    {
        if(m_pad.localPosition.x < m_rightBorder)
        {
            float jump = m_speed * Time.deltaTime;
            m_pad.Translate(new Vector3(jump, 0, 0), Space.World);
        }
        
    }
    public void M_MoveLeft()
    {
        if (m_pad.localPosition.x > m_leftBorder)
        {
            float jump = m_speed * Time.deltaTime;
            m_pad.Translate(new Vector3(-jump, 0, 0), Space.World);
        }
        
    }
}
