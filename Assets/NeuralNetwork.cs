﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralNetwork 
{
    public List<Neuron> m_input;
    public List<Neuron> m_output;
    public List<List<Neuron>> m_hiddenLayer;

    public void M_CreateNetwork(int input, int output, List<int> layers)
    {
        m_input = new List<Neuron>();
        m_output = new List<Neuron>();
        m_hiddenLayer = new List<List<Neuron>>();

        m_input = new List<Neuron>();
        for (int i = 0; i < input; i++)
            m_input.Add(new Neuron());

        for (int i = 0; i < layers.Count; i ++)
        {
            m_hiddenLayer.Add(new List<Neuron>());
            for (int j = 0; j < layers[i]; j++)
            {
                m_hiddenLayer[i].Add(new Neuron());
            }
        }

        m_output = new List<Neuron>();
        for (int i = 0; i < output; i++)
            m_output.Add(new Neuron());

        M_LinkNeurons();
    }

    public void M_LinkNeurons()
    {
        for(int i = 0; i< m_hiddenLayer.Count; i++)
        {
            if(i == 0)
            {
                for(int j = 0; j < m_hiddenLayer[i].Count; j++)
                {
                    for(int h = 0; h < m_input.Count; h++)
                    {
                        m_hiddenLayer[i][j].m_input.Add(m_input[h]);
                        m_hiddenLayer[i][j].m_inputValue.Add(0);
                    }
                }
            }
            else
            {
                for (int j = 0; j < m_hiddenLayer[i].Count; j++)
                {
                    for (int h = 0; h < m_hiddenLayer[i-1].Count; h++)
                    {
                        m_hiddenLayer[i][j].m_input.Add(m_hiddenLayer[i - 1][h]);
                        m_hiddenLayer[i][j].m_inputValue.Add(0);
                    }
                }
            }
        }

        for (int i = 0; i < m_output.Count; i++)
        {
            for(int j = 0; j < m_hiddenLayer[m_hiddenLayer.Count-1].Count; j++)
            {
                m_output[i].m_input.Add(m_hiddenLayer[m_hiddenLayer.Count - 1][j]);
                m_output[i].m_inputValue.Add(0);
            }
        }
    }

    public void M_RandomiseValues()
    {
        foreach(Neuron obj in m_input)
        {
            for (int i = 0; i < obj.m_inputValue.Count; i++)
            {
                obj.m_inputValue[i] = (Random.value * 2) - 1;
            }
            obj.bias = (Random.value * 10) - 5;
        }

        foreach(List<Neuron> list in m_hiddenLayer)
            foreach(Neuron obj in list)
            {
                for (int i = 0; i < obj.m_inputValue.Count; i++)
                {
                    obj.m_inputValue[i] = (Random.value * 200) - 100;
                }
                obj.bias = (Random.value * 200) - 100;
            }

        foreach (Neuron obj in m_output)
        {
            for (int i = 0; i < obj.m_inputValue.Count; i++)
            {
                obj.m_inputValue[i] = (Random.value * 200) - 100;
            }
            obj.bias = (Random.value * 200) - 100;
        }
    }

    public void M_CalculateNetwork()
    {
        foreach (List<Neuron> list in m_hiddenLayer)
            foreach (Neuron obj in list)
                obj.M_CalculateValue();
        foreach (Neuron obj in m_output)
            obj.M_CalculateValue();
    }
}
