﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    public float m_speedIncrement;
    private void OnCollisionExit(Collision collision)
    {
        GetComponent<Rigidbody>().AddForce(GetComponent<Rigidbody>().velocity.normalized * m_speedIncrement);
    }
}
