﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public PadScript m_pad;
    public KeyCode m_left;
    public KeyCode m_right;

    private void Update()
    {
        if (Input.GetKey(m_left)) m_pad.M_MoveLeft();
        if (Input.GetKey(m_right)) m_pad.M_MoveRight();
    }
}
