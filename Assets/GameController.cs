﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public Vector2 m_mapSize;
    public Vector2 m_padSize;
    public float m_padSpeed;
    public float m_ballInitialSpeed;
    public float m_speedIncrement;
    public float m_zaxis;

    public GameObject m_borderPrefab;
    public GameObject m_padPrefab;
    public GameObject m_ballPrefab;
    public Transform m_anchor;

    public GameObject m_pad1;
    public GameObject m_pad2;
    public GameObject m_ball;

    public float m_upperPadPos;
    public float m_lowerPadPos;

    public Vector2 m_ballPos;
    public Vector2 m_ballVelocityVector;
    public Vector2 m_ballVelocity;
    public Vector2 m_pad1Pos;
    public Vector2 m_pad2Pos;

    private void Awake()
    {
        m_zaxis = transform.localPosition.z;
        M_Setup();
        M_SetupPlayers();
    }
    private void Update()
    {
        M_CalculateBallPosition();
        M_TransferPositionData();
        M_iterateAI();
    }

    public void M_Setup()
    {
        m_ball = Instantiate(m_ballPrefab, m_anchor);
        m_pad1 = Instantiate(m_padPrefab, m_anchor);
        m_pad2 = Instantiate(m_padPrefab, m_anchor);

        m_ball.transform.localPosition = new Vector3(0, 0, m_zaxis);
        m_pad1.transform.localPosition = new Vector3(0, 0, m_zaxis);
        m_pad2.transform.localPosition = new Vector3(0, 0, m_zaxis);
        m_pad1.transform.localScale = new Vector3(m_padSize.x, m_padSize.y, 1);
        m_pad2.transform.localScale = new Vector3(m_padSize.x, m_padSize.y, 1);

        m_pad1Pos = new Vector2(0, (m_mapSize.y + m_padSize.y) / 2);
        m_pad2Pos = new Vector2(0, (m_mapSize.y + m_padSize.y) / -2);

        m_ballVelocity = (m_ballVelocityVector.normalized) * m_ballInitialSpeed;

        M_TransferPositionData();
    }
    public void M_TransferPositionData()
    {
        m_ball.transform.localPosition = new Vector3(m_ballPos.x, m_ballPos.y, m_zaxis);
        m_pad1.transform.localPosition = new Vector3(m_pad1Pos.x, m_pad1Pos.y, m_zaxis);
        m_pad2.transform.localPosition = new Vector3(m_pad2Pos.x, m_pad2Pos.y, m_zaxis);
    }
    public void M_CalculateBallPosition()
    {
        m_ballPos += m_ballVelocity;
        if (m_ballPos.y > m_mapSize.y / 2 || m_ballPos.y < -m_mapSize.y / 2)
        {
            if(m_ballPos.y > m_mapSize.y / 2)
            {
                m_ballVelocity.y = m_ballVelocity.y * -1;
                if(m_ballPos.x >= m_pad1Pos.x + (m_padSize.x/2) || m_ballPos.x <= m_pad1Pos.x - (m_padSize.x / 2))
                {
                    m_player1.M_RandomiseValues();
                }
            }
            if(m_ballPos.y < -m_mapSize.y / 2)
            {
                m_ballVelocity.y = m_ballVelocity.y * -1;
                if (m_ballPos.x >= m_pad2Pos.x + (m_padSize.x / 2) || m_ballPos.x <= m_pad2Pos.x - (m_padSize.x / 2))
                {
                    m_player2.M_RandomiseValues();
                }
            }
        }
            
        if (m_ballPos.x > m_mapSize.x / 2 || m_ballPos.x < -m_mapSize.x / 2)
            m_ballVelocity.x = m_ballVelocity.x * -1;
    }


    public NeuralNetwork m_player1;
    public NeuralNetwork m_player2;

    public void M_SetupPlayers()
    {
        m_player1 = new NeuralNetwork();
        m_player1.M_CreateNetwork(2, 2, new List<int> { 2 });
        m_player1.M_RandomiseValues();

        m_player2 = new NeuralNetwork();
        m_player2.M_CreateNetwork(2, 2, new List<int> { 8, 16, 8 });
        m_player2.M_RandomiseValues();
    }
    public void M_iterateAI()
    {
        float dataToInput1;
        float dataToInput2;

        if(m_pad1Pos.x >= m_ballPos.x)
        {
            dataToInput1 = 1;
            dataToInput2 = 0;
        }
        else
        {
            dataToInput1 = 0;
            dataToInput2 = 1;
        }
        m_player1.m_input[0].value = dataToInput1;
        m_player1.m_input[1].value = dataToInput2;

        m_player1.M_CalculateNetwork();

        int direction = 0;

        if (m_player1.m_output[0].value >= 0.9) direction--;
        if (m_player1.m_output[1].value >= 0.9) direction++;

        M_MovePlayer1(direction);
        ////
        if (m_pad2Pos.x >= m_ballPos.x)
        {
            dataToInput1 = 1;
            dataToInput2 = 0;
        }
        else
        {
            dataToInput1 = 0;
            dataToInput2 = 1;
        }
        m_player2.m_input[0].value = dataToInput1;
        m_player2.m_input[1].value = dataToInput2;

        m_player2.M_CalculateNetwork();

        direction = 0;

        if (m_player2.m_output[0].value >= 0.9) direction--;
        if (m_player2.m_output[1].value >= 0.9) direction++;

        M_MovePlayer2(direction);
    }
    public void M_MovePlayer1(int direction)
    {
        if (direction == -1 && m_pad1Pos.x >= ((-m_mapSize.x/2)+(m_padSize.x/2))) m_pad1Pos.x -= m_padSpeed;
        if (direction == 1 && m_pad1Pos.x <= ((m_mapSize.x / 2)-(m_padSize.x/2))) m_pad1Pos.x += m_padSpeed;
    }
    public void M_MovePlayer2(int direction)
    {
        if (direction == -1 && m_pad2Pos.x >= ((-m_mapSize.x / 2) + (m_padSize.x / 2))) m_pad2Pos.x -= m_padSpeed;
        if (direction == 1 && m_pad2Pos.x <= ((m_mapSize.x / 2) - (m_padSize.x / 2))) m_pad2Pos.x += m_padSpeed;
    }
    
}
