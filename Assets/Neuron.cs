﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Neuron
{
    public List<Neuron> m_input;
    public List<float> m_inputValue;

    public float bias;

    public float value;

    public void M_CalculateValue()
    {
        value = bias;
        for (int i = 0; i < m_input.Count; i++)
            value += m_input[i].value * m_inputValue[i];
        value = M_Sigmoid(value);
    }
    public float M_Sigmoid(float valueToCalculate)
    {
        return 1 / (1 + Mathf.Exp(-valueToCalculate));
    }

    public Neuron()
    {
        m_input = new List<Neuron>();
        m_inputValue = new List<float>();
        bias = 0;
        value = 0;
    }
}
